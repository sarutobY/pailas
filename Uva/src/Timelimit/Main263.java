package Timelimit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author JEFFERSON
 */
public class Main263 {

    public static void main(String[] args) {
        Scanner leerDatos = new Scanner(System.in);
        String casos = "";
        while (!(casos = leerDatos.next()).equals("0")) {
            ArrayList<Integer> resultados = new ArrayList<Integer>();
            int contador = 0;
            System.out.println("Original number was " + casos);
            while (true) {
                ArrayList aux1 = new ArrayList();
                ArrayList aux2 = new ArrayList();
                for (int i = 0; i < casos.length(); i++) {
                    aux1.add(casos.charAt(i));
                }
                Collections.sort(aux1);
                for (int i = aux1.size() - 1; i >= 0; i--) {
                    aux2.add(aux1.get(i));
                }
                String n1 = "";
                String n2 = "";
                for (int i = 0; i < aux1.size(); i++) {
                    n1 += aux1.get(i);
                    n2 += aux2.get(i);
                }
                int resultado = Integer.valueOf(n2) - Integer.valueOf(n1);
                contador++;
                casos = String.valueOf(resultado);
                String aux = "";
                if ((n1.charAt(0) != '0') && n1.length() != 1) {
                    aux += n1.charAt(0);
                }
                if (n1.length() == 1) {
                    aux += n1.charAt(0);
                }
                aux+=n1.substring(1);
                System.out.println(n2 + " - " + aux + " = " + resultado);
                if (resultados.contains(resultado)) {
                    break;
                } else {
                    resultados.add(resultado);
                }
            }
            System.out.println("Chain length " + contador);
            System.out.println("");
        }
    }
}
