
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author JEFFERSON
 */
public class Main10901No {

    public static void main(String[] args) {
        Scanner leerDatos = new Scanner(System.in);
        Queue<Integer> left = new LinkedList();
        Queue<Integer> right = new LinkedList();
        int n = leerDatos.nextInt();
        int t = leerDatos.nextInt();
        int m = leerDatos.nextInt();
        for (int i = 0; i < m; i++) {
            int a = leerDatos.nextInt();
            String b = leerDatos.next();
            if (b.equals("left")) {
                left.add(a);
            } else {
                right.add(a);
            }
        }
        ArrayList<Integer> resultados = new ArrayList<>();
        String ubicacion = "left";
        int contador = t;
        while (!left.isEmpty() || !right.isEmpty()) {
            int minimo = 0;
            if (left.isEmpty()) {
                minimo = right.element();
            } else if (right.isEmpty()) {
                minimo = left.element();
            } else {
                minimo = min(left.element(), right.element());
            }
            System.out.println("minimo " + minimo);

            if (minimo == left.element()) {
                if (ubicacion.equals("right")) {
                    contador += t;
                }
                ubicacion = "right";
                while (left.element() >= contador) {
                    contador += t;
                    System.out.println("contador left: " + contador);
                }

                for (int i = 0; i < n; i++) {
                    if (!left.isEmpty()) {
                        if (left.element() <= contador) {
                            if (i == 0) {
                                contador += t;
                            }
                            left.remove();
                            System.out.println(contador);
                        } else {
                            break;
                        }
                    }
                }

            } else {
                if (ubicacion.equals("left")) {
                    contador += t;
                }
                ubicacion = "left";
                while (right.element() >= contador) {
                    contador += t;
                    System.out.println("contador right: " + contador);
                }
                for (int i = 0; i < n; i++) {
                    if (!right.isEmpty()) {
                        if (right.element() <= contador) {
                            if (i == 0) {
                                contador += t;
                            }
                            right.remove();
                            System.out.println(contador);
                        } else {
                            break;
                        }
                    }
                }

            }

        }

        System.out.println("left " + left);
        System.out.println("rigt " + right);
    }

    public static int min(int a, int b) {
        int minimo = a;
        if (minimo > b) {
            minimo = b;
        }
        return minimo;
    }

}
