
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author JEFFERSON
 */
public class Main13191 {

    public static void main(String[] args) {
        Scanner leerDatos = new Scanner(System.in);
        int casos = leerDatos.nextInt();
        for (int i = 0; i < casos; i++) {
            ArrayList<Integer> datos = new ArrayList<>();
            int c = leerDatos.nextInt();
            int contador = 2;
            while (c > 1) {
                if (isPrime(contador) == true) {
                    if (c % contador == 0) {
                        c = c / contador;
                        datos.add(contador);
                        contador = 1;
                    }
                }
                contador++;
            }
            String resultado = "";
            for (int j = 0; j < datos.size() - 1; j++) {
                resultado += datos.get(j) + " ";
            }
            resultado += datos.get(datos.size() - 1);
            System.out.println(resultado);

        }

    }

    public static void primosSerie(int a) {
        ArrayList<Integer> primos = new ArrayList();
        primos.add(0, 2);
        for (int i = 1; i < a; i++) {
            int k = (int) primos.get(primos.size() - 1) + 1;
            for (int l = 0; l < primos.size(); l++) {
                if (k % (int) primos.get(l) == 0) {
                    l = -1;
                    k++;
                }
            }
            primos.add(i, k);
        }
        for (int i = 0; i < primos.size(); i++) {
            System.out.println(primos.get(i));
        }
    }

    public static boolean isPrime(int n) {
        ArrayList<Integer> primos = new ArrayList();
        primos.add(0, 2);
        if (n == 2) {
            return true;
        }
        for (int i = 1; (int) primos.get(i - 1) < n; i++) {
            int k = (int) primos.get(primos.size() - 1) + 1;
            for (int l = 0; l < primos.size(); l++) {
                if (k % (int) primos.get(l) == 0) {
                    l = -1;
                    k++;
                }
            }
            if (k == n) {
                return true;
            }
            primos.add(i, k);
        }
        return false;
    }
}
