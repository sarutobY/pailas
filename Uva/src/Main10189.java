
import java.util.Arrays;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author jeffe
 */
public class Main10189 {

    public static void main(String[] args) {
        Scanner leerDatos = new Scanner(System.in);
        int n;
        int m;
        int veces = 0;
        while ((n = leerDatos.nextInt()) != 0 && (m = leerDatos.nextInt()) != 0) {
            veces++;
            String[][] matriz = new String[n][m];
            //tablero
            for (int i = 0; i < n; i++) {
                String palabra = leerDatos.next();
                for (int j = 0; j < m; j++) {
                    if (palabra.charAt(j) == '*') {
                        matriz[i][j] = String.valueOf(palabra.charAt(j));
                    } else {
                        matriz[i][j] = "0";
                    }
                }
            }

            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    if (matriz[i][j].equals("*")) {
                        int auxi = 0;
                        int auxif = 1;
                        if (i != 0) {
                            auxi = i - 1;
                            auxif = auxi + 2;
                        }
                        if (i == n - 1) {
                            auxi = n - 2;
                            auxif = auxi + 1;
                        }
                        for (int k = auxi; k <= auxif; k++) {
                            int auxj = 0;
                            int auxjf = 1;
                            if (j != 0) {
                                auxj = j - 1;
                                auxjf = auxj + 2;
                            }
                            if (j == m - 1) {
                                auxj = m - 2;
                                auxjf = auxj + 1;
                            }

                            for (int l = auxj; l <= auxjf; l++) {
                                if (!matriz[k][l].equals("*")) {
                                    int numero = Integer.parseInt(matriz[k][l]);
                                    numero++;
                                    matriz[k][l] = "" + numero;
                                }
                            }
                        }

                    }

                }
            }
            String resultado = "";
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    resultado += matriz[i][j];
                }
                resultado += "\n";
            }
            System.out.println("Field #" + veces + ":");
            System.out.println(resultado);
        }
    }

}
