
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author JEFFERSON
 */
public class Main10327 {

    public static void main(String[] args) {
        Scanner leerDatos = new Scanner(System.in);
        while (leerDatos.hasNext()) {
            ArrayList<Integer> a = new ArrayList<Integer>();
            ArrayList<Integer> b = new ArrayList<Integer>();
            int casos = leerDatos.nextInt();
            for (int i = 0; i < casos; i++) {
                int dato = leerDatos.nextInt();
                a.add(dato);
                b.add(dato);
            }
            Collections.sort(a);
            int resultado = 0;
            for (int i = 0; i < a.size(); i++) {
                if (a.get(i) == b.get(i)) {
                    resultado++;
                }
            }
            System.out.println("Minimum exchange operations : " + (2 * resultado));
        }
    }
}
