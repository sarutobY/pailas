
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author JEFFERSON
 */
public class Main160 {

    public static void main(String[] args) {
        Scanner leerDatos = new Scanner(System.in);
        while (leerDatos.hasNext()) {
            int dato = leerDatos.nextInt();
            if (dato == 0) {
                System.exit(0);
            }
            int[] primos = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 53, 59};
            int[] resultados = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

            BigInteger resultadof = factorial(new BigInteger("" + BigInteger.valueOf(dato)));
            System.out.println(resultadof);
            while (resultadof.compareTo(BigInteger.ONE)>0) {
                for (int i = 0; i < primos.length; i++) {
                    int a = primos[i];
                    System.out.println("ingreso al for");
                    if (resultadof.mod(new BigInteger(""+a)).equals(BigInteger.ZERO)) {
                        System.out.println("ingreso al if");
                        resultadof = resultadof.divide(new BigInteger(""+a));
                        int c = resultados[i];
                        c++;
                        resultados[i] = c;
                        break;
                    }
                }
            }
            String resultado = dato + "!" + " =";
            for (int i = 0; i < resultados.length; i++) {
                if (resultados[i] != 0) {
                    resultado += "  " + resultados[i];
                }
            }

            System.out.println(resultado);
        }
    }

    static BigInteger factorial(BigInteger dato) {
        if (dato.equals(BigInteger.ZERO)) {
            return BigInteger.ONE;
        } else {
            return dato.multiply(factorial(dato.subtract(BigInteger.ONE)));
        }
    }
}
