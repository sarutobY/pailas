
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author silver
 */
class Main10346 {

    public static void main(String[] args) {
        Scanner leerDatos = new Scanner(System.in);
        while (leerDatos.hasNext()) {
            int n = leerDatos.nextInt();
            int k = leerDatos.nextInt();
            int colillas = 0;
            int total = n;
            while (n > 0) {
                colillas += n % k;
                n = n / k;
                total += n;
                if (colillas >= k) {
                    total++;
                    colillas = colillas - k;
                    n++;
                }
            }
            System.out.println(total);
        }

    }
}
