
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author JEFFERSON
 */
public class Main21078 {

    public static void main(String[] args) {
        Scanner leerDatos = new Scanner(System.in);
        int nCasos = leerDatos.nextInt();
        for (int l = 0; l < nCasos; l++) {
            leerDatos.nextLine();
            int nEstaciones = leerDatos.nextInt();
            int nInterseciones = leerDatos.nextInt();
            LinkedList estaciones = new LinkedList();
            GraphAdyList grafo = new GraphAdyList(nInterseciones, false);
            grafo.insertVertex(nInterseciones);
            for (int i = 0; i < nEstaciones; i++) {
                int ubicacionEstacion = leerDatos.nextInt();
                estaciones.add(i, ubicacionEstacion - 1);
            }
            if (nInterseciones == 1) {
                System.out.println(1);
                System.out.println("");
            } else {
                for (int i = 0; i < nInterseciones; i++) {
                    int x = leerDatos.nextInt();
                    int y = leerDatos.nextInt();
                    int peso = leerDatos.nextInt();
                    grafo.insertEdgeWeightInt(x - 1, y - 1, peso);
                }

                LinkedList matriz = new LinkedList(); //Todos los caminos
                matriz = grafo.matrizAdyacenciaWeightm();
                grafo.floidWarshal(matriz);

                LinkedList estacionesMin = new LinkedList();
                LinkedList auxiliar = (LinkedList) matriz.get((int) estaciones.get(0));
                for (int i = 0; i < matriz.size(); i++) {
                    estacionesMin.add(i, auxiliar.get(i));
                }

                for (int i = 1; i < estaciones.size(); i++) {
                    LinkedList aux = (LinkedList) matriz.get((int) estaciones.get(i));
                    for (int j = 0; j < estacionesMin.size(); j++) {
                        estacionesMin.set(j, menor((int) estacionesMin.get(j), (int) aux.get(j)));
                    }
                }

                LinkedList matrizFinal = new LinkedList();
                for (int i = 0; i < matriz.size(); i++) {
                    LinkedList aux1 = (LinkedList) matriz.get(i);
                    LinkedList aux2 = new LinkedList();
                    for (int j = 0; j < aux1.size(); j++) {
                        aux2.add(j, menor((int) estacionesMin.get(j), (int) aux1.get(j)));
                    }
                    matrizFinal.add(i, aux2);
                }
                LinkedList resultado = new LinkedList();
                for (int i = 0; i < matrizFinal.size(); i++) {
                    LinkedList auxiliarcito = (LinkedList) matrizFinal.get(i);
                    int mayor = (int) auxiliarcito.get(0);
                    for (int j = 1; j < auxiliarcito.size(); j++) {
                        if ((int) auxiliarcito.get(j) > mayor) {
                            mayor = (int) auxiliarcito.get(j);
                        }
                    }
                    resultado.add(i, mayor);
                }
                int pos = 1;
                int menor = (int) resultado.get(0);
                for (int i = 1; i < resultado.size(); i++) {
                    if ((int) resultado.get(i) < menor) {
                        menor = (int) resultado.get(i);
                        pos = i + 1;
                    }
                }
                System.out.println(pos);
                System.out.println("");

            }
        }
    }

    /**
     * Este metodo me permite obtener el dato menor de dos int
     *
     * @param a int: primer dato a comparar
     * @param b int: segundo dato a comparar
     * @return int: es el dato menor de los dos datos ingresados
     */
    public static int menor(int a, int b) {
        int menor = a;
        if (menor > b) {
            menor = b;
        }
        return menor;
    }

    static class GraphAdyList implements Graph {

        boolean diGraph;
        int maxVertex;
        int numVertex;
        List listAdy[];
        int raiz[]; // arreglo auxiliar para hacer el metodo de kruskal
        LinkedList aristas; //Chain que almacena los edges creados para ser utilizado en el metodo de kruskal

        /**
         * Constructor
         *
         * @param d boolean: isdiGrap
         */
        public GraphAdyList(boolean d) {
            maxVertex = numVertex = 0;
            diGraph = d;
        }

        /**
         * Constructor
         *
         * @param n inr: maxVertex
         * @param d boolean; isdiGrap
         */
        public GraphAdyList(int n, boolean d) {
            diGraph = d;
            maxVertex = n;
            numVertex = 0;
            listAdy = new List[n];
            aristas = new LinkedList();
        }

        public void insertVertex(int n) {
            if (n > maxVertex - numVertex) {
                System.out.println("Error");
            } else {
                for (int i = numVertex; i < numVertex + n; i++) {
                    listAdy[i] = new List();
                }
                numVertex += n;
            }
        }

        public void deleteVertex(int v) {

            if (v > listAdy.length) {
                System.out.println("Error");
            } else {
                listAdy[v].firstNode = null;
                for (int i = 0; i < listAdy.length; i++) {
                    if (existEdge(i, v)) {
                        deleteEdge(i, v);
                    }
                }
            }
        }

        public void insertEdge(int i, int j) {
            if (i >= numVertex && j >= numVertex) {
                System.out.println("Error");
            } else {
                listAdy[i].add(listAdy[i].size, j);
                if (!diGraph) {
                    listAdy[j].add(listAdy[j].size, i);
                }
            }

        }

        /**
         * Este metodo permite insertar un edge con un peso entre dos nodos,
         * teniendo el peso con un valor double
         *
         * @param i int: nodo de inicio
         * @param j int: nodo final
         * @param peso int:peso entre los dos nodos
         */
        public void insertEdgeWeightInt(int i, int j, int peso) {
            if (i >= numVertex && j >= numVertex) {
                System.out.println("Error");
            } else {
                listAdy[i].add(listAdy[i].size, new Arco(j, (int) peso));
                if (!diGraph) {
                    listAdy[j].add(listAdy[j].size, new Arco(i, (int) peso));
                }
            }
        }

        /**
         * Este metodo permite insertar un edge con un peso entre dos nodos,
         * teniendo el peso con un valor double
         *
         * @param i int: nodo de inicio
         * @param j int: nodo final
         * @param peso double:peso entre los dos nodos
         */
        public void insertEdgeWeightDouble(int i, int j, double peso) {
            if (i >= numVertex && j >= numVertex) {
                System.out.println("Error");
            } else {
                aristas.add(aristas.size(), new Edge(i, j, peso));
                listAdy[i].add(listAdy[i].size, new Arco(j, (double) peso));
                if (!diGraph) {
                    listAdy[j].add(listAdy[j].size, new Arco(i, (double) peso));
                }
            }
        }

        public void deleteEdge(int i, int j) {
            if (i >= numVertex && j >= numVertex) {
                System.out.println("Error");
            } else {
                listAdy[i].remove(listAdy[i].indexOf(j));
                if (!diGraph) {
                    if (listAdy[j].indexOf(i) != -1) {
                        listAdy[j].remove(listAdy[j].indexOf(i));
                    }
                }
            }
        }

        public boolean isEmpty() {

            for (int i = 0; i < listAdy.length; i++) {
                if (listAdy[i] != null) {
                    return false;
                }
            }
            return true;

        }

        public boolean existEdge(int i, int j) {
            if (listAdy[i] == null) {
                return false;
            }
            ListNode aux = listAdy[i].firstNode;
            while (aux != null) {
                if (aux.element.equals(j)) {
                    return true;
                }
                aux = aux.next;
            }
            return false;
        }

        public int degreeIn(int v) {
            int dgIn = 0;
            for (int i = 0; i < numVertex; i++) {
                if (i != v) {
                    if (listAdy[i].indexOf(v) != -1) {
                        dgIn++;
                    }
                }
            }
            return dgIn;
        }

        public int degreeOut(int i) {
            int dgOut = 0;
            ListNode aux = listAdy[i].firstNode;
            while (aux != null) {
                dgOut++;
                aux = aux.next;
            }
            return dgOut;
        }

        public int incidence(int i) {
            if (!diGraph) {
                return degreeIn(i);
            } else {
                return degreeIn(i) + degreeOut(i);
            }
        }

        public int size() {
            int size = 0;
            for (int i = 0; i < numVertex; i++) {
                size += numElementos(listAdy[i]);
            }
            if (!diGraph) {
                size = size / 2;
            }
            return size;
        }

        public int numElementos(List lista) {
            ListNode aux = lista.firstNode;
            int resul = 0;
            while (aux != null) {
                resul += 1;
                aux = aux.next;
            }
            return resul;
        }

        /**
         * Me permite imprimir la estructura
         */
        public void printGraph() {
            System.out.println("Number maximum of vertices: " + maxVertex + "\n");
            System.out.println("the graph has " + numVertex + " vertex: \n");
            for (int i = 0; i < numVertex; i++) {
                System.out.print("Vertex " + i + ": ");
                toString(listAdy[i]);
            }
        }

        /**
         * Permite obtener todos los valores de la lista y almacenarlos en un
         * String
         *
         * @param lista List: posee toda la informacion de la estructura
         */
        public void toString(List lista) {
            ListNode aux;
            aux = lista.firstNode;
            String str = "";
            while (aux != null) {
                str += aux.element + ", ";
                aux = aux.next;
            }
            if (str.length() > 1) {
                str = str.substring(0, str.length() - 2);
            }
            System.out.println("[" + str + "]");
        }

        /**
         * Permite inmprimir el grafo con pesos de valores enteros
         */
        public void printGraphWeightInt() {
            System.out.println("Number maximum of vertices: " + maxVertex + "\n");
            System.out.println("the graph has " + numVertex + " vertex: \n");
            for (int i = 0; i < numVertex; i++) {
                System.out.print("Vertex " + i + ": ");
                toStringWeightInt(listAdy[i]);
            }
        }

        /**
         * Permite obtener todos los valores de la lista y almacenarlos en un
         * String
         *
         * @param lista List: posee toda la informacion de la estructura
         */
        public void toStringWeightInt(List lista) {
            ListNode aux;
            aux = lista.firstNode;
            String str = "";
            while (aux != null) {
                Arco a = (Arco) aux.element;
                str += (int) a.destino + "(" + (int) a.peso + ")" + ", ";
                aux = aux.next;
            }
            if (str.length() > 1) {
                str = str.substring(0, str.length() - 2);
            }
            System.out.println("[" + str + "]");
        }

        /**
         * Permite inmprimir el grafo con pesos de valores double
         */
        public void printGraphWeightDouble() {
            System.out.println("Number maximum of vertices: " + maxVertex + "\n");
            System.out.println("the graph has " + numVertex + " vertex: \n");
            for (int i = 0; i < numVertex; i++) {
                System.out.print("Vertex " + i + ": ");
                toStringWeightInt(listAdy[i]);
            }
        }

        /**
         * Permite obtener todos los valores de la lista y almacenarlos en un
         * String
         *
         * @param lista List: posee toda la informacion de la estructura
         */
        public void toStringWeightDouble(List lista) {
            ListNode aux;
            aux = lista.firstNode;
            String str = "";
            while (aux != null) {
                Arco a = (Arco) aux.element;
                str += (int) a.destino + "(" + (double) a.peso + ")" + ", ";
                aux = aux.next;
            }
            if (str.length() > 1) {
                str = str.substring(0, str.length() - 2);
            }
            System.out.println("[" + str + "]");
        }

        /**
         * Este metodo permite obtener la matriz de adyacencia de un grafo sin
         * pesos
         *
         * @return Chain que hace referencia a la matriz de adyacencia generada
         * con la estructura
         */
        public LinkedList matrizdeAdyacencia() {
            LinkedList matriz = new LinkedList();
            for (int i = 0; i < numVertex; i++) {
                LinkedList matrizaux = new LinkedList();
                ListNode aux = listAdy[i].firstNode;
                for (int j = 0; j < numVertex; j++) {
                    matrizaux.add(j, 0);
                }
                while (aux != null) {
                    if ((int) matrizaux.get((int) aux.element) == 0) {
                        matrizaux.set((int) aux.element, 1);
                        aux = aux.next;
                    } else {
                        matrizaux.set((int) aux.element, (int) matrizaux.get((int) aux.element) + 1);
                        aux = aux.next;
                    }
                }
                matriz.add(i, matrizaux);
            }
            return matriz;
        }

        /**
         * Este metodo permite obtener la matriz de adyacencia de un grafo con
         * pesos
         *
         * @return Chain que hace referencia a la matriz de adyacencia generada
         * con la estructura
         */
        public LinkedList matrizAdyacenciaWeightm() {
            LinkedList matriz = new LinkedList();
            for (int i = 0; i < numVertex; i++) {
                LinkedList matrizaux = new LinkedList();
                ListNode aux = listAdy[i].firstNode;
                for (int j = 0; j < numVertex; j++) {
                    if (i == j) {
                        matrizaux.add(j, 0);
                    } else {
                        matrizaux.add(j, 99999);
                    }
                }
                while (aux != null) {
                    Arco dato = (Arco) aux.element;
                    if ((int) matrizaux.get((int) dato.destino) == 99999) {
                        matrizaux.set((int) dato.destino, dato.peso);
                        aux = aux.next;
                    }
                }
                matriz.add(i, matrizaux);
            }
            return matriz;
        }

        /**
         * Este metodo me permite obtener todos los caminos minimos partiendo de
         * todos los nodos del grafo
         *
         * @param matriz Chain que hace referencia a la matriz de adyacencia
         */
        public void floidWarshal(LinkedList matriz) {
            LinkedList pi = new LinkedList();
            for (int i = 0; i < matriz.size(); i++) {
                LinkedList aux = (LinkedList) matriz.get(i);
                LinkedList aux2 = new LinkedList();
                for (int j = 0; j < aux.size(); j++) {
                    if (i == j || (int) aux.get(j) == 99999) {
                        aux2.add(j, -1);
                    } else {
                        aux2.add(j, (i + 1));
                    }
                }
                pi.add(i, aux2);
            }
            for (int k = 0; k < numVertex; k++) {
                LinkedList auxk = (LinkedList) matriz.get(k);
                LinkedList auxpik = (LinkedList) pi.get(k);
                for (int i = 0; i < numVertex; i++) {
                    LinkedList auxi = (LinkedList) matriz.get(i);
                    LinkedList auxpii = (LinkedList) pi.get(i);
                    for (int j = 0; j < numVertex; j++) {
                        int a = (int) auxi.get(j);
                        auxi.set(j, minimo((int) auxi.get(j), (int) auxi.get(k), (int) auxk.get(j)));
                        matriz.set(i, auxi);
                        if (a != (int) auxi.get(j)) {
                            auxpii.set(j, auxpik.get(j));
                            pi.set(i, auxpii);
                        }

                    }
                }
            }
        }

        /**
         * Este metodo me permite hallar el minimo entre dos numeros, a y b+c
         *
         * @param a int: primer numero
         * @param b int: segundo numero
         * @param c int:tercer numero
         * @return int: es el dato minimo entre a y b+c
         */
        public int minimo(int a, int b, int c) {
            int menor = 0;
            int suma = b + c;
            menor = a;
            if (menor >= suma) {
                menor = suma;
            }
            return menor;
        }

        /**
         * Para la implementacion del metodo de kruskal nos guiamos de las
         * siguientes paginas:
         * https://jariasf.wordpress.com/2012/04/19/arbol-de-expansion-minima-algoritmo-de-kruskal/
         * https://jariasf.wordpress.com/2012/04/02/disjoint-set-union-find/
         *
         * @return double que es el peso del arbol de expancion minima
         */
        public double kruskal() {
            iniciarRaiz(maxVertex);
            insertionSort(aristas);
            double pesos = 0;
            for (int i = 0; i < aristas.size(); i++) {
                Edge arista = (Edge) aristas.get(i);
                if (mismaComponente(arista.origen, arista.destino) == false) {
                    union(arista.origen, arista.destino);
                    pesos += arista.getPeso();
                }
            }
            return pesos;
        }

        /**
         * Este metodo me permite ordenar un Chain
         *
         * @param a Chain matriz que va a ser ordenada
         */
        public static void insertionSort(LinkedList a) {
            for (int i = 1; i < a.size(); i++) {
                // insert a[i] into a[0:i-1]
                Comparable t = (Comparable) a.get(i);
                // find proper place for t
                int j;
                for (j = i - 1; j >= 0 && (t.compareTo(a.get(j)) < 0); j--) {
                    a.set(j + 1, a.get(j));
                }
                a.set(j + 1, t);
            }
        }

        /**
         * permite imprimir los valores almacenados en el arreglo raiz
         */
        public void mostrarpadre() {
            for (int i = 0; i < raiz.length; i++) {
                System.out.println(raiz[i]);
            }
        }

        /**
         * permite inicializa el arreglo raiz
         *
         * @param n int: que hace referencia al numero de nodos presentes en el
         * grafo
         */
        public void iniciarRaiz(int n) {
            raiz = new int[n];
            for (int i = 0; i < n; ++i) {
                raiz[i] = i;
            }
        }

        /**
         * permite saber si dos nodos estan en la misma componente
         *
         * @param x nodo x
         * @param y nodo y
         * @return true si estan en la misma componente, false en otro caso
         */
        public boolean mismaComponente(int x, int y) {
            if (buscarRaiz(x) == buscarRaiz(y)) {
                return true;
            }
            return false;
        }

        /**
         * Permite unir dos nodos en el arreglo de raiz
         *
         * @param x nodo x
         * @param y nodo y
         */
        public void union(int x, int y) {
            int raizX = buscarRaiz(x);
            int raizY = buscarRaiz(y);
            raiz[raizX] = raizY;
        }

        /**
         * Este metodo me permite conocer la raiz de un nodo
         *
         * @param x nodo x
         * @return int: que hace referencia al nodo raiz del nodo ingresado
         */
        public int buscarRaiz(int x) {
            if (x == raiz[x]) {
                return x;
            } else {
                return buscarRaiz(raiz[x]);
            }
        }

        /**
         * Este metodo me permite determinar el camino mas corto dado un vertice
         * origen al resto de los vertices de un grafo
         *
         * @param nodoInicio int que hacer refencial al nodo donde se inicia el
         * algoritmo
         */
        public void dijkstra(int nodoInicio) {
            Double d[] = new Double[numVertex];
            Integer π[] = new Integer[numVertex];
            boolean q[] = new boolean[numVertex];
            for (int i = 0; i < numVertex; i++) {
                d[i] = Double.POSITIVE_INFINITY;
                π[i] = null;
                q[i] = true;
            }
            d[nodoInicio] = 0.0;
            for (int i = 0; i < numVertex; i++) {
                int pos = buscarMenor(d, q);
                ListNode a = listAdy[pos].firstNode;
                while (a != null) {
                    Arco pes = (Arco) a.element;
                    double peso = (double) pes.peso;
                    if (d[pes.destino] > (d[pos] + peso)) {
                        d[pes.destino] = (d[pos] + peso);
                        π[pes.destino] = pos;
                    }
                    a = a.next;
                }
                if (cntBoolean(q) == q.length) {
                    break;
                }
            }
            System.out.println("RESULTADO FINAL");
            System.out.println(" --d--");
            imprimirArray(d);
            System.out.println("---π--");
            imprimirArray(π);
        }

        /**
         * Metodo auxiliar del dijkstra
         *
         * @param b array de datos booleans
         * @return int que hace referencia a la cantidad de tatos false que ay
         * en un array de booleans
         */
        public int cntBoolean(boolean b[]) {
            int cnt = 0;
            for (int i = 0; i < b.length; i++) {
                if (b[i] == false) {
                    cnt++;
                }
            }
            return cnt;
        }

        /**
         * Metodo auxiliar del dijkstra
         *
         * @param a [] array de datos Double
         * @param b [] array de datos boolean
         * @return int que hace referencia a la posicion donde se encuentra el
         * dato menor
         */
        public int buscarMenor(Double a[], boolean b[]) {
            int pos = 0;
            double menor = menortrue(a, b);
            for (int i = 0; i < a.length; i++) {
                if (menor >= a[i]) {
                    if (b[i] == true) {
                        menor = a[i];
                        pos = i;
                    }
                }
            }
            b[pos] = false;
            return pos;
        }

        /**
         * Metodo auxiliar del dijkstra
         *
         * @param a [] array de datos Double
         * @param b [] array de datos booleans
         * @return int que hace referencia a la posicion donde se encuentra el
         * dato menor y no se ha utilizado
         */
        public double menortrue(Double a[], boolean b[]) {
            double menor = 0;
            for (int i = 0; i < b.length; i++) {
                if (b[i] == true) {
                    menor = a[i];
                    break;
                }
            }
            return menor;
        }

        /**
         * este metodo me permite imprimir un array de Integers
         *
         * @param a Integer: arreglo que sera impreso
         */
        public void imprimirArray(Integer a[]) {
            String valor = "";
            for (int i = 0; i < a.length; i++) {
                if (a[i] == null) {
                    valor += "*" + ", ";
                } else {
                    valor += a[i] + ", ";
                }
            }
            if (valor.length() > 1) {
                valor = valor.substring(0, valor.length() - 2);
            }
            System.out.println("[" + valor + "]");
        }

        /**
         * Permite imprimir un array de Double
         *
         * @param a [] array de datos Double
         */
        public void imprimirArray(Double a[]) {
            String valor = "";
            for (int i = 0; i < a.length; i++) {
                if (a[i] == null) {
                    valor += "*" + ", ";
                } else {
                    valor += a[i] + ", ";
                }
            }
            if (valor.length() > 1) {
                valor = valor.substring(0, valor.length() - 2);
            }
            System.out.println("[" + valor + "]");
        }

        /**
         * perimite imprimir un array de booleans
         *
         * @param a [] array de datos boolean
         */
        public void imprimirArrayBoolean(boolean a[]) {
            String valor = "";
            for (int i = 0; i < a.length; i++) {
                if (a[i] == true) {
                    valor += "t" + ", ";
                } else {
                    valor += "f" + ", ";
                }
            }
            if (valor.length() > 1) {
                valor = valor.substring(0, valor.length() - 2);
            }
            System.out.println("[" + valor + "]");
        }

    }

    static interface Graph {

        /**
         * permite insertar los vertices
         *
         * @param n int: numero de vertices a insertar
         */
        public void insertVertex(int n);

        /**
         * Permite eliminar un vertice
         *
         * @param v int: numero del vertice a eliminar
         */
        public void deleteVertex(int v);

        /**
         * permite insertar un Edge
         *
         * @param i int: numero del Edge de inicio
         * @param j int: numero del Edge del final
         */
        public void insertEdge(int i, int j);

        /**
         * permite eliminar un Edge
         *
         * @param i int: numero del Edge de inicio
         * @param j int: numero del Edge del final
         */
        public void deleteEdge(int i, int j);

        /**
         * permite saber si la estructura es vacia
         *
         * @return true si es vacia false en otro caso
         */
        public boolean isEmpty();

        /**
         * permite saber si existe una conexion entre dos edges
         *
         * @param i int: numero del Edge de inicio
         * @param j int: numero del Edge del final
         * @return true si existe un edge entre los dos nodos, false en otro
         * caso
         */
        public boolean existEdge(int i, int j);

        /**
         *
         * @param i nodo a consultar
         * @return int: cantidad de nodos de entrada
         */
        public int degreeIn(int i);

        /**
         *
         * @param i nodo a consultar
         * @return int: cantidad de nodos de salidad
         */
        public int degreeOut(int i);

        /**
         *
         * @param i nodo a consultar
         * @return int: cantidad de nodos de salida mas los nodos de entrada
         */
        public int incidence(int i);

        /**
         * permite saber el numero de vertices que existe en la estructura
         *
         * @return int: es el tamaño del numVertex en la estructura, 0 si esta
         * esta vacia
         */
        public int size();

    }

    static class List implements LinearList {

        protected ListNode firstNode;
        protected int size;

        public List(int initialCapacity) {
        }

        public List() {
            this(0);
        }

        public boolean isEmpty() {

            return size == 0;
        }

        public int size() {
            return size;
        }

        void checkIndex(int index) {
            if (index < 0 || index >= size) {
                throw new IndexOutOfBoundsException("index = " + index + " size = " + size);
            }
        }

        public Object get(int index) {
            checkIndex(index);
            ListNode currentNode = firstNode;
            for (int i = 0; i < index; i++) {
                currentNode = currentNode.next;
            }

            return currentNode.element;
        }

        public int indexOf(Object theElement) {
            ListNode currentNode = firstNode;
            int index = 0;
            while (currentNode != null && !currentNode.element.equals(theElement)) {
                currentNode = currentNode.next;
                index++;
            }
            if (currentNode == null) {
                return -1;
            } else {
                return index;
            }
        }

        public String toString() {
            StringBuffer s = new StringBuffer("[");

            // put elements into the buffer
            ListNode currentNode = firstNode;

            while (currentNode != null) {
                if (currentNode.element == null) {
                    s.append("null, ");
                } else {
                    s.append(currentNode.element.toString().concat(", "));
                }
                currentNode = currentNode.next;
            }

            // remove last ", "
            if (size > 0) {
                s.delete(s.length() - 2, s.length());
            }

            s.append("]");

            // create equivalent String
            return new String(s);

        }

        public Object remove(int index) {
            checkIndex(index);
            Object removedElement;
            if (index == 0) // remove first node
            {
                removedElement = firstNode.element;
                firstNode = firstNode.next;
            } else {
                // use q to get to predecessor of desired node
                ListNode q = firstNode;
                for (int i = 0; i < index - 1; i++) {
                    q = q.next;
                }
                removedElement = q.next.element;
                q.next = q.next.next; // remove desired node
            }
            size--;
            return removedElement;
        }

        public void add(int index, Object theElement) {
            // invalid list position
            if (index < 0 || index > size) {
                throw new IndexOutOfBoundsException("index = " + index + " size = " + size);
            }

            if (index == 0) // insert at front
            {
                firstNode = new ListNode(index, theElement, firstNode);
            } else { // find predecessor of new element
                ListNode p = firstNode;
                for (int i = 0; i < index - 1; i++) {
                    p = p.next;
                }
                // insert after p
                p.next = new ListNode(index, theElement, p.next);
            }
            size++;
        }
    }

    static class ListNode {

        int id;
        Object element;
        ListNode next;

        /**
         * constructor sin parametros
         */
        ListNode() {
        }

        /**
         * Constructor con parametros de la clase
         *
         * @param id id
         * @param element element
         */
        ListNode(int id, Object element) {
            this.id = id;
            this.element = element;
        }

        /**
         * Constructor con parametros de la clase
         *
         * @param id id
         * @param element element
         * @param next next
         */
        ListNode(int id, Object element, ListNode next) {
            this.id = id;
            this.element = element;
            this.next = next;
        }

    }

    static interface LinearList {

        /**
         * Este metodo nos permite saber si una lista esta vacia Return
         *
         * @return true if the list is empty. false otherwise
         */
        public boolean isEmpty();

        /**
         * Este metodo nos permite saber el tamaño de la lista
         *
         * @return the number of elements in the list
         */
        public int size();

        /**
         * Obtiene un elemento de la lista dado un indice
         *
         * @param index int: que hace referencia a la ubicacion donde se buscara
         * el objeto
         * @return Object the index-th element of the list
         */
        public Object get(int index);

        /**
         * muestra el indice de la consulta de un elemento en la lista
         *
         * @param theElement Object : elemento que se va a buscar en la lista
         * @return the position of the first occurrence of x in the list, return
         * -1 if x is not in the list
         */
        public int indexOf(Object theElement);

        /**
         * Remueve un elemento de la lista
         *
         * @param index int: ubicacion del elemento en la lista que sera
         * eliminado
         * @return the index-th element, elements with higher index have their
         * index reduced by 1
         */
        public Object remove(int index);

        /**
         * Insert x as the index-th element, elements with index mayor o igua
         * index have their index increased by 1
         *
         * @param index int: posicion donde va a ser agregado el nuevo elemento
         * @param theElement Object: Elemento que se ubicara sobre la lista
         */
        public void add(int index, Object theElement);

        /**
         * Este metodo permite imprimir la lista
         *
         * @return String: Output the list elements from left to right
         */
        public String toString();

    }

    static class Edge implements Comparable<Edge> {

        public int origen;
        public int destino;
        public double peso;

        /**
         * Constructor de la clase Edge
         *
         * @param origen int: origen
         * @param destino int: destino
         * @param peso double: peso
         */
        public Edge(int origen, int destino, double peso) {
            this.origen = origen;
            this.destino = destino;
            this.peso = peso;
        }

        /**
         * permite obtener el valor del edge de origen
         *
         * @return int: valor del edge de origen
         */
        public int getOrigen() {
            return origen;
        }

        /**
         * permite modificar el valor del edge de origen
         *
         * @param origen int: nuevo valor del edge de origen
         */
        public void setOrigen(int origen) {
            this.origen = origen;
        }

        /**
         * permite obtener el valor del edge destino
         *
         * @return int: valor del edge destino
         */
        public int getDestino() {
            return destino;
        }

        /**
         * permite modificar el valor del edge destino
         *
         * @param destino int: nuevo valor del edge destino
         */
        public void setDestino(int destino) {
            this.destino = destino;
        }

        /**
         * permite obtener el valor del edge peso
         *
         * @return valor del edge peso
         */
        public double getPeso() {
            return peso;
        }

        /**
         * permite modificar el valor del edge peso
         *
         * @param peso int: nuevo valor del edge peso
         */
        public void setPeso(double peso) {
            this.peso = peso;
        }

        /**
         * Metodo implementado de la interface comparable que me permite en caso
         * de ordenar los objetos de la clase hacerlo por peso
         *
         * @param t Objeto de la clase Edge el cual va a se comparado
         * @return -1 si el peso del objeto ingresado es mayor que el peso del
         * otro objeto comparado 1 si el peso del objeto ingresado es menor que
         * el costo del otro objeto comparado, o en otro caso
         *
         */
        @Override
        public int compareTo(Edge t) {
            if (peso < t.peso) {
                return -1;
            }
            if (peso > t.peso) {
                return 1;
            }
            return 0;
        }

    }

    static class Arco {

        public int destino;
        public Object peso;

        /**
         * Contructor de la clase Arco
         *
         * @param destino int destino
         * @param peso Object peso
         */
        public Arco(int destino, Object peso) {
            this.destino = destino;
            this.peso = peso;
        }

        /**
         * permite obtener el valor del destino
         *
         * @return int: valor del destino
         */
        public int getDestino() {
            return destino;
        }

        /**
         * permite modificar el valor del destino
         *
         * @param destino int: nuevo valor del destino
         */
        public void setDestino(int destino) {
            this.destino = destino;
        }

        /**
         * permite obtener el valor del peso
         *
         * @return int: valor del peso
         */
        public Object getPeso() {
            return peso;
        }

        /**
         * permite modificar el valor del peso
         *
         * @param peso int: nuevo valor del peso
         */
        public void setPeso(Object peso) {
            this.peso = peso;
        }

    }
}
