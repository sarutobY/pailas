
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author JEFFERSON
 */
public class Main11498 {

    public static void main(String[] args) {
        FastReader leerDatos = new FastReader();
        int a = 0;
        while ((a = leerDatos.nextInt()) != 0) {
            int x = leerDatos.nextInt();
            int y = leerDatos.nextInt();
            for (int i = 0; i < a; i++) {
                int x1 = leerDatos.nextInt();
                int y1 = leerDatos.nextInt();
                int c = x1 - x;
                int b = y1 - y;
                if (c > 0 && b > 0) {
                    System.out.println("NE");
                } else if (c < 0 && b < 0) {
                    System.out.println("SO");
                } else if (c < 0 && b > 0) {
                    System.out.println("NO");
                } else if (c > 0 && b < 0) {
                    System.out.println("SE");
                } else {
                    System.out.println("divisa");
                }
            }

        }

    }

    static class FastReader {

        BufferedReader br;
        StringTokenizer st;

        public FastReader() {
            br = new BufferedReader(new InputStreamReader(System.in));
        }

        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }

        int nextInt() {
            return Integer.parseInt(next());
        }

        long nextLong() {
            return Long.parseLong(next());
        }

        double nextDouble() {
            return Double.parseDouble(next());
        }

        String nextLine() {
            String str = "";
            try {
                str = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return str;
        }
    }
}
