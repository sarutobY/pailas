
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author JEFFERSON
 */
public class Main13191dos {

    public static void main(String[] args) {
        Scanner leerDatos = new Scanner(System.in);
        int casos = leerDatos.nextInt();
        for (int i = 0; i < casos; i++) {
            ArrayList<BigInteger> datos = new ArrayList<>();
            String d = leerDatos.next();
            BigInteger c = new BigInteger(d);
            BigInteger contador = new BigInteger("" + 2);
            while (c.compareTo(BigInteger.ONE) > 0) {
                if (isPrime(contador) == true) {
                    if (c.mod(contador).equals(new BigInteger("" + 0))) {
                        c = c.divide(contador);
                        datos.add(contador);
                        contador = new BigInteger("" + 1);
                    }
                }
                contador = contador.add(new BigInteger("" + 1));
            }
            String resultado = "";
            for (int j = 0; j < datos.size() - 1; j++) {
                resultado += datos.get(j) + " ";
            }
            resultado += datos.get(datos.size() - 1);
            System.out.println(resultado);

        }

    }

    public static boolean isPrime(BigInteger n) {
        ArrayList<BigInteger> primos = new ArrayList();
        primos.add(0, new BigInteger("" + 2));
        if (n.equals(new BigInteger("" + 2))) {
            return true;
        }
        for (int i = 1; (primos.get(i - 1).compareTo(n)) < 0; i++) {
            BigInteger k = primos.get(primos.size() - 1).add(BigInteger.ONE);
            for (int l = 0; l < primos.size(); l++) {
                if (k.mod(primos.get(l)).equals(new BigInteger("" + 0))) {
                    l = -1;
                    k = k.add(new BigInteger("" + 1));
                }
            }
            if (k.equals(n)) {
                return true;
            }
            primos.add(i, k);
        }
        return false;
    }
}
