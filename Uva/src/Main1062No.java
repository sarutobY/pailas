
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import java.util.Stack;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author JEFFERSON
 */
public class Main1062No {

    public static void main(String[] args) {
        Scanner leerDatos = new Scanner(System.in);
        String dato = "";
        while (!(dato = leerDatos.next()).equals("end")) {
            Stack<Character> pila = new Stack<Character>();
            ArrayList<Stack<Character>> contenedores = new ArrayList<Stack<Character>>();
            ArrayList<Character> barcos = new ArrayList<Character>();
            for (int i = 0; i < dato.length(); i++) {
                pila.push(dato.charAt(i));
                if (!barcos.contains(dato.charAt(i))) {
                    barcos.add(dato.charAt(i));
                }
            }
            contenedores.add(pila);
            Collections.sort(barcos);

            for (int i = 0; i < barcos.size(); i++) {
                while (pila.search(barcos.get(i)) != -1) {
                    if (pila.peek().equals(barcos.get(i))) {
                        pila.pop();
                    } else {
                        boolean encontrado = false;
                        for (int j = 1; j < contenedores.size(); j++) {
                            Stack<Character> aux = (Stack<Character>) contenedores.get(j);
                            if (aux.peek().equals(pila.peek())) {
                                aux.push(pila.peek());
                                contenedores.set(j, aux);
                                encontrado = true;
                            }
                        }
                        if (encontrado == false) {
                            Stack<Character> nuevo = new Stack<Character>();
                            nuevo.push(pila.peek());
                            contenedores.add(nuevo);
                        }
                        pila.pop();
                    }

                }
            }
            System.out.println(contenedores.size());
        }

    }

    static void imprimirarray(ArrayList<Stack<Character>> aux) {
        for (int i = 0; i < aux.size(); i++) {
            Stack<Character> aux2 = (Stack<Character>) aux.get(i);
            System.out.println(aux2);
        }

    }

}
