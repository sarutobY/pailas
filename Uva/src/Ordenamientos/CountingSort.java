/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ordenamientos;

import java.util.Arrays;

/**
 *
 * @author jeffersondario
 */
public class CountingSort {
    public static void main(String[] args) {
        int[] dato = {10, 9, 8, 7, 6, 5, 5, 4, 5, 3, 2, 1, 0};
        Counting_Sort(dato);
        System.out.println(Arrays.toString(dato));
    }
    /**
     * este metodo me permite ordenar un array list
     *
     * @param imput array dinamico el cual se va a ordenar con el metodo
     */
    public static void Counting_Sort(int[] imput) {
        int[] index = new int[(max(imput) - min(imput))+1];
        int[] count = new int[index.length];
        int[] sumCount = new int[count.length];
        int[] sortedImput = new int[imput.length];
        int pos = 0;
        for (int i = min(imput); i <= max(imput); i++) {
            index[pos] = i;
            pos++;
        }
        for (int i = 0; i < index.length; i++) {
            int val = 0;
            for (int k = 0; k < imput.length; k++) {
                if (index[i] == (int) imput[k]) {
                    val++;
                }
            }
            count[i] = val;
        }
        sumCount[0] = count[0];
        for (int i = 1; i < count.length; i++) {
            sumCount[i] = sumCount[i - 1] + count[i];
        }

        for (int i = 0; i < imput.length; i++) {
            sortedImput[i] = imput[i];
        }

        for (int i = 0; i < sortedImput.length; i++) {
            int ubic = 0;
            for (int j = 0; j < index.length; j++) {
                if (sortedImput[i] == index[j]) {
                    ubic = sumCount[j];
                    sumCount[j] = ubic - 1;
                    break;
                }
            }
            imput[ubic - 1] = sortedImput[i];
        }
    }

    /**
     *
     * @param a Array al cual se obtedra el elemento mayor
     * @return int el cual me dice cual es el elemento mayor
     */
    public static int max(int[] a) {
        int mayor = 0;
        mayor = (int) a[0];
        for (int i = 0; i < a.length; i++) {
            if ((int) a[i] > mayor) {
                mayor = (int) a[i];
            }
        }
        return mayor;
    }

    /**
     *
     * @param b Array al cual se obtedra el elemento menor
     * @return int el cual me dice cual es el elemento menor
     */
    public static int min(int[] b) {
        int menor = 0;
        menor = b[0];
        for (int i = 0; i < b.length; i++) {
            if (b[i] < menor) {
                menor = (int) b[i];
            }
        }
        return menor;
    }

}
