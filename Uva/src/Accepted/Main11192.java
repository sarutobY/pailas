package Accepted;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author jeffersondario
 */
public class Main11192 {
    
    public static void main(String[] args) {
        FastReader leerDatos = new FastReader();
        int particion;
        while ((particion = leerDatos.nextInt()) != 0) {
            String dato = leerDatos.next();
            String resultado = "";
            int veces = dato.length() / particion;
            int inicio = 1;
            int datofinal = veces;
            for (int i = 0; i < particion; i++) {
                String substring = dato.substring(inicio - 1, datofinal);
                for (int j = substring.length() - 1; j >= 0; j--) {
                    resultado += substring.charAt(j);
                }
                inicio += veces;
                datofinal += veces;
            }
            System.out.println(resultado);
        }
        System.exit(0);
    }
    
    static class FastReader {
        
        BufferedReader br;
        StringTokenizer st;
        
        public FastReader() {
            br = new BufferedReader(new InputStreamReader(System.in));
        }
        
        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }
        
        int nextInt() {
            return Integer.parseInt(next());
        }
        
        long nextLong() {
            return Long.parseLong(next());
        }
        
        double nextDouble() {
            return Double.parseDouble(next());
        }
        
        String nextLine() {
            String str = "";
            try {
                str = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return str;
        }
    }
}
