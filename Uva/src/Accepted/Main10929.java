package Accepted;


import java.math.BigInteger;
import java.util.Scanner;

class Main10929 {

    public static void main(String[] args) {
        Scanner leerDatos = new Scanner(System.in);
        String a = "";
        while (!(a = leerDatos.nextLine()).equals("0")) {
            a = a.trim();
            BigInteger dato = new BigInteger(a);
            if (dato.mod(BigInteger.valueOf(11)).equals(BigInteger.ZERO)) {
                System.out.println(a + " is a multiple of 11.");
            } else {
                System.out.println(a + " is not a multiple of 11.");
            }

        }
    }
}
