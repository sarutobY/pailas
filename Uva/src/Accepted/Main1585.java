package Accepted;


import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author jeffe
 */
public class Main1585 {

    public static void main(String[] args) {
        Scanner leerDatos = new Scanner(System.in);
        int datos = leerDatos.nextInt();
        String b;
        while (datos-- > 0) {
            b = leerDatos.next();
            int[] arreglo = new int[b.length()];
            int total = 0;

            if (b.charAt(0) == 'O') {
                arreglo[0] = 1;
                total += 1;
            } else {
                arreglo[0] = 0;
            }
            for (int i = 1; i < b.length(); i++) {
                if (b.charAt(i) == 'O') {
                    arreglo[i] = arreglo[i - 1] + 1;
                    total += arreglo[i];
                } else {
                    arreglo[i] = 0;
                }

            }
            System.out.println(total);
        }
    }
}
