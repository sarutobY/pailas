package Accepted;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author JEFFERSON
 */
public class Main458 {

    public static void main(String[] args) throws IOException {
        BufferedReader ls = new BufferedReader(new InputStreamReader(System.in));
        String cadena;
        while ((cadena = ls.readLine()) != null) {
            String resultado = "";
            for (int i = 0; i < cadena.length(); i++) {
                int s = cadena.codePointAt(i) - 7;
                resultado += (char) s;
            }
            System.out.println(resultado);
        }
    }

    /**
     * public static void main(String[] args) throws IOException {
     * DataInputStream in = new DataInputStream(System.in); DataOutputStream out
     * = new DataOutputStream(System.out);
     *
     * int codigo; while ((codigo = in.read()) != -1) {
     * out.write((Character.isSpace((char) codigo) ? codigo : (codigo - 7))); }
     * }
     */
}
