package Accepted;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author JEFFERSON
 */
public class Main12100 {

    public static void main(String[] args) {
        FastReader leerDatos = new FastReader();
        int casos = leerDatos.nextInt();
        StringBuilder imprimir = new StringBuilder();
        for (int i = 0; i < casos; i++) {
            Queue<Integer> cola = new LinkedList<Integer>();
            int tamano = leerDatos.nextInt();
            int ubicacion = leerDatos.nextInt();
            for (int j = 0; j < tamano; j++) {
                cola.add(leerDatos.nextInt());
            }
            int resultado = 0;
            int posMayor = -1;
            while (true) {
                int mayor = 0;
                posMayor = 0;
                for (int j = 0; j < cola.size(); j++) {
                    if (cola.element() > mayor) {
                        mayor = cola.element();
                        posMayor = j;
                    }
                    cola.add(cola.poll());
                }
                for (int j = 0; j < posMayor; j++) {
                    cola.add(cola.poll());
                }
                if (ubicacion < posMayor) {
                    ubicacion = (tamano - posMayor) + ubicacion;
                    ubicacion = ubicacion - 1;
                    cola.remove();
                    tamano = tamano - 1;
                } else if (ubicacion > posMayor) {
                    ubicacion = ubicacion - posMayor - 1;
                    cola.remove();
                    tamano = tamano - 1;
                } else {
                    ubicacion = 0;
                    cola.remove();
                    tamano = tamano - 1;
                    resultado++;
                    break;
                }
                resultado++;
            }
            imprimir.append(resultado).append("\n");
        }
        imprimir.deleteCharAt(imprimir.length() - 1);
        System.out.println(imprimir);
    }

    static class FastReader {

        BufferedReader br;
        StringTokenizer st;

        public FastReader() {
            br = new BufferedReader(new InputStreamReader(System.in));
        }

        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }

        int nextInt() {
            return Integer.parseInt(next());
        }

        long nextLong() {
            return Long.parseLong(next());
        }

        double nextDouble() {
            return Double.parseDouble(next());
        }

        String nextLine() {
            String str = "";
            try {
                str = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return str;
        }
    }

}
