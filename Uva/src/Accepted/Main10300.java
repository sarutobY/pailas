package Accepted;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author jeffersondario
 */
public class Main10300 {

    public static void main(String[] args) {
        FastReader leerDatos = new FastReader();
        int casos = leerDatos.nextInt();
        for (int i = 0; i < casos; i++) {
            int numeroGranjeros = leerDatos.nextInt();
            long resultado = 0;
            for (int j = 0; j < numeroGranjeros; j++) {
                long a = leerDatos.nextInt();
                long b = leerDatos.nextInt();
                long c = leerDatos.nextInt();
                if (b != 0) {
                    resultado += (a * c);
                }

            }
            System.out.println(resultado);
        }
        System.exit(0);
    }

    static class FastReader {

        BufferedReader br;
        StringTokenizer st;

        public FastReader() {
            br = new BufferedReader(new InputStreamReader(System.in));
        }

        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }

        int nextInt() {
            return Integer.parseInt(next());
        }

        long nextLong() {
            return Long.parseLong(next());
        }

        double nextDouble() {
            return Double.parseDouble(next());
        }

        String nextLine() {
            String str = "";
            try {
                str = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return str;
        }
    }
}
