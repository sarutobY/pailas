package Accepted;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author jeffersondario
 */
public class Main91035 {

    public static void main(String[] args) throws IOException {
        BufferedReader leerDatos = new BufferedReader(new InputStreamReader(System.in));
        String dato = "";
        StringBuilder r = new StringBuilder();
        while (!(dato = leerDatos.readLine()).equals("0")) {
            Queue<Integer> cola = new LinkedList<Integer>();
            int n = Integer.parseInt(dato);
            for (int i = 1; i <= n; i++) {
                cola.add(i);
            }
            r.append("Discarded cards:");
            while (cola.size() > 1) {
                r.append(" ").append(cola.remove()).append(",");
                cola.add(cola.remove());
            }
            if (!dato.equals("1")) {
                r.deleteCharAt(r.length() - 1);
            }
            r.append("\n");
            r.append("Remaining card: ").append(cola.remove()).append("\n");
        }
        System.out.print(r);
    }
}
