package Accepted;


import java.math.BigInteger;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author jeffe
 */
public class Main374 {

    public static void main(String[] args) {
        Scanner leerDatos = new Scanner(System.in);
        while (leerDatos.hasNext()) {
            BigInteger B = leerDatos.nextBigInteger();
            BigInteger P = leerDatos.nextBigInteger();
            BigInteger M = leerDatos.nextBigInteger();
            BigInteger s = B.modPow(P, M);
            System.out.println(s);
        }
    }
}
