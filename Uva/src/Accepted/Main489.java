package Accepted;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author jefferson
 */
public class Main489 {

    public static void main(String[] args) throws IOException {
        BufferedReader leerDatos = new BufferedReader(new InputStreamReader(System.in));
        String round = "";
        while (!(round = leerDatos.readLine()).equals("-1")) {
            String solucion = leerDatos.readLine();
            String intento = leerDatos.readLine();
            int contador = 0;
            String aux = "";
            for (int i = 0; i < intento.length(); i++) {
                if (contador == 7) {
                    break;
                }
                if (solucion.contains("" + intento.charAt(i))) {
                    solucion = solucion.replace("" + intento.charAt(i), "");
                } else {
                    if (!aux.contains("" + intento.charAt(i))) {
                        contador++;
                    }
                }
                aux += "" + intento.charAt(i);
            }
            System.out.println("Round " + round);
            if (contador >= 7 && !solucion.isEmpty()) {
                System.out.println("You lose.");
            } else {
                if (solucion.isEmpty()) {
                    System.out.println("You win.");
                } else {
                    System.out.println("You chickened out.");
                }
            }
        }
    }
}
