package Accepted;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author jeffe
 */
class Main12289 {

    public static void main(String[] args) throws IOException {
        BufferedReader lx = new BufferedReader(new InputStreamReader(System.in));
        int datos = Integer.parseInt(lx.readLine());
        for (int i = 0; i < datos; i++) {
            String palabra = lx.readLine();
            if (palabra.length() == 5) {
                System.out.println("3");
            } else {
                int uno = 0;
                int dos = 0;
                String a = "one";
                String b = "two";
                for (int j = 0; j < 3; j++) {
                    if (a.charAt(j) == palabra.charAt(j)) {
                        uno++;
                    }
                    if (b.charAt(j) == palabra.charAt(j)) {
                        dos++;
                    }
                }
                if (uno > dos) {
                    System.out.println("1");
                } else {
                    System.out.println("2");
                }
            }
        }

    }
}
