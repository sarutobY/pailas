package Accepted;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main12149 {

    public static void main(String[] args) throws IOException {
        BufferedReader leerDatos = new BufferedReader(new InputStreamReader(System.in));
        String casos = "";
        while (!(casos = leerDatos.readLine()).equals("0")) {
            int n = Integer.parseInt(casos);
            int resultado = (n * (n + 1) * ((2 * n) + 1)) / 6;
            System.out.println(resultado);
        }
    }

}
