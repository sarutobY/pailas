package Accepted;


import java.math.BigInteger;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author jefferson
 */
public class Main10023 {

    public static void main(String[] args) {
        Scanner leerDatos = new Scanner(System.in);
        int casos = leerDatos.nextInt();
        while (casos-- > 0) {
            BigInteger dato = leerDatos.nextBigInteger();
            System.out.println(raiz(dato));
            if (casos > 0) {
                System.out.println();
            }
        }
    }

    public static BigInteger raiz(BigInteger x) {
        BigInteger a = BigInteger.ZERO.setBit(x.bitLength() / 2);
        BigInteger b = a;
        while (true) {
            BigInteger c = a.add(x.divide(a)).shiftRight(1);
            if (c.equals(a) || c.equals(b)) {
                return c;
            }
            b = a;
            a = c;
        }
    }
}
