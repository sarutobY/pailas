package Accepted;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author jefferson
 */
public class Main949 {

    public static void main(String[] args) throws IOException {
        BufferedReader leerDatos = new BufferedReader(new InputStreamReader(System.in));
        String dato = "";
        while ((dato = leerDatos.readLine()) != null) {
            String info = dato.replaceAll("[^A-Za-z]", " ");
            info = info.trim();
            String[] info2 = info.split("\\s+");
            System.out.println(info2.length);
        }
    }
}
