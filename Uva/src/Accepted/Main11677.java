package Accepted;


import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author jeffe
 */
public class Main11677 {

    public static void main(String[] args) {
        Scanner leerDatos = new Scanner(System.in);
        int h1 = 0, m1 = 0, h2 = 0, m2 = 0;
        while (leerDatos.hasNext()) {
            h1 = leerDatos.nextInt();
            m1 = leerDatos.nextInt();
            h2 = leerDatos.nextInt();
            m2 = leerDatos.nextInt();
            if (h1 == 0 && m1 == 0 && h2 == 0 && m2 == 0) {
                break;
            }
            int minutosi = 60 - m1;
            int hora;
            if (h2 >= (h1 + 1)) {
                hora = (h2 - (h1 + 1)) * 60;
            } else {
                hora = ((h2 - (h1 + 1)) + 24) * 60;
            }
            if (h1 == h2 && m1 < m2) {
                System.out.println(m2-m1);
            } else {
                System.out.println(hora + minutosi + m2);
            }
        }
    }
}
