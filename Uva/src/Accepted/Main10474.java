package Accepted;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author jeffersondario
 */
public class Main10474 {

    public static void main(String[] args) {
        FastReader leerDatos = new FastReader();
        int n = 0, q = 0;
        int casos = 1;
        StringBuilder resultado = new StringBuilder();
        while ((n = leerDatos.nextInt()) != 0 && (q = leerDatos.nextInt()) != 0) {
            int[] datos = new int[n];
            for (int i = 0; i < n; i++) {
                datos[i] = leerDatos.nextInt();
            }
            datos = sort(datos);
            resultado.append("CASE# ").append(casos).append(":").append("\n");
            for (int i = 0; i < q; i++) {
                int busqueda = leerDatos.nextInt();
                int pos = buscar(datos, busqueda);
                if (pos != -1) {
                    resultado.append(busqueda).append(" found at ").append(pos + 1).append("\n");
                } else {
                    resultado.append(busqueda).append(" not found").append("\n");
                }
            }
            casos++;
        }
        System.out.print(resultado);
    }

    static int buscar(int[] arreglo, int dato) {
        int pos = -1;
        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] == dato) {
                pos = i;
                break;
            }
        }
        return pos;
    }

    public static int[] sort(int[] array) {
        int[] aux = new int[array.length];
        int min = array[0];
        int max = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] < min) {
                min = array[i];
            } else if (array[i] > max) {
                max = array[i];
            }
        }
        int[] counts = new int[max - min + 1];
        for (int i = 0; i < array.length; i++) {
            counts[array[i] - min]++;
        }
        counts[0]--;
        for (int i = 1; i < counts.length; i++) {
            counts[i] = counts[i] + counts[i - 1];
        }
        for (int i = array.length - 1; i >= 0; i--) {
            aux[counts[array[i] - min]--] = array[i];
        }

        return aux;
    }

    static class FastReader {

        BufferedReader br;
        StringTokenizer st;

        public FastReader() {
            br = new BufferedReader(new InputStreamReader(System.in));
        }

        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }

        int nextInt() {
            return Integer.parseInt(next());
        }

        long nextLong() {
            return Long.parseLong(next());
        }

        double nextDouble() {
            return Double.parseDouble(next());
        }

        String nextLine() {
            String str = "";
            try {
                str = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return str;
        }
    }

}
