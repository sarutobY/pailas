package Accepted;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author jeffersondario
 */
public class Main10107 {

    public static void main(String[] args) throws IOException {
        Scanner leerDatos = new Scanner(System.in);
        ArrayList<Integer> numeros = new ArrayList<>();
        while (leerDatos.hasNext()) {
            numeros.add(leerDatos.nextInt());
            Collections.sort(numeros);
            int pos = numeros.size() / 2;
            if (numeros.size() % 2 == 0) {
                System.out.println((numeros.get(pos - 1) + numeros.get(pos)) / 2);
            } else {
                System.out.println(numeros.get(pos));
            }
        }
    }
}
